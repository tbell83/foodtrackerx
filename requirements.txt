Flask >= 0.10.1
Flask-Login
Flask-Migrate
Flask-SQLAlchemy
Flask-Uploads
Flask-WTF
PyMySQL
black
flake8