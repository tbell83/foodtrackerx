from setuptools import setup

setup(
    name='food.tombell.io',
    version='0.1',
    packages=['app'],
    zip_safe=False,
    install_requires=[
        'Flask>=0.10.1',
        'Flask-Login',
        'Flask-Migrate',
        'Flask-SQLAlchemy',
        'Flask-Uploads',
        'Flask-WTF',
        'PyMySQL'
    ]
)
