from app import DB
from flask_login import UserMixin


class Dish(DB.Model):
    id = DB.Column(DB.Integer, primary_key=True)
    name = DB.Column(DB.String(128))
    description = DB.Column(DB.String(1024))
    recipe = DB.Column()
    # source = DB.Column(DB.String(512))


class Recipe(DB.Model):
    id = DB.Column(DB.Integer, primary_key=True)
    name = DB.Column(DB.String(128))
    description = DB.Column(DB.String(1024))
    steps = DB.Column(DB.Text)
    ingredient = DB.relationship("IngredientAmount", backref="ingredient", lazy=True)


class Ingredient(DB.Model):
    id = DB.Column(DB.Integer, primary_key=True)
    name = DB.Column(DB.String(128))
    quantity = DB.Column(DB.Integer)
    unit = DB.Column(DB.String(16))


class IngredientAmount(DB.Model):
    id = DB.Column(DB.Integer, primary_key=True)
    ingredient_id = DB.Column(
        DB.Integer, DB.ForeignKey("ingredient.id"), nullable=False
    )
    amount = DB.Column(DB.Integer)
    recipe_id = DB.Column(DB.Integer, DB.ForeignKey("recipe.id"), nullable=False)


class User(UserMixin, DB.Model):
    about_me = DB.Column(DB.String(280))
    email = DB.Column(DB.String(128), index=True, unique=True)
    id = DB.Column(DB.Integer, primary_key=True)
    last_seen = DB.Column(DB.DateTime, default=datetime.utcnow)
    password_hash = DB.Column(DB.String(128))
    username = DB.Column(DB.String(64), index=True, unique=True)

    def __repr__(self):
        return "<User {}>".format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode("utf-8")).hexdigest()
        return "https://www.gravatar.com/avatar/{}?d=identicon&s={}".format(
            digest, size
        )


@LOGIN.user_loader
def load_user(id):
    return User.query.get(int(id))
