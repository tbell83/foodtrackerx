from __future__ import print_function
from os import getenv, path, getcwd
import json


class Config(object):
    def __init__(self):
        self.ALLOWED_EXTENSIONS = ['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif']
        self.APPLICATION_PATH = '/'.join(getcwd().split('/')[1:]).split('/')
        self.DB_HOST = None
        self.DB_PORT = None
        self.DB_USER = None
        self.DB_PASS = None
        self.DB_TYPE = None
        self.DB_NAME = None
        self.INTERPRETER = path.join(
            path.sep, *(self.APPLICATION_PATH+['env', 'bin', 'python']))
        self.MYSQL_DATABASE_DB = None
        self.MYSQL_DATABASE_PORT = 3389
        self.MYSQL_DATABASE_HOST = None
        self.MYSQL_DATABASE_PASSWORD = None
        self.MYSQL_DATABASE_USER = None
        self.SECRET_KEY = None
        self.STATIC_FOLDER = path.join(
            path.sep, *(self.APPLICATION_PATH + ['static']))
        self.UPLOAD_FOLDER = path.join(
            path.sep, *(self.APPLICATION_PATH + ['images']))
        self.DB_TYPE = None
        self.SQLALCHEMY_DATABASE_URI = None
        self.SQLALCHEMY_TRACK_MODIFICATIONS = False

    def load(self, json_settings):
        allowed_keys = [
            'SECRET_KEY',
            'DB_USER',
            'DB_PASS',
            'DB_NAME',
            'DB_PORT',
            'DB_HOST',
            'DB_TYPE',
            'APPLICATION_PATH',
            'UPLOAD_FOLDER',
            'ALLOWED_EXTENSIONS',
            'INTERPRETER'
        ]
        with open(json_settings) as infile:
            data = json.load(infile)
            self.__dict__.update(
                (k, v) for k, v in data.iteritems() if k in allowed_keys)

        self.SQLALCHEMY_DATABASE_URI = '{}://{}:{}@{}:{}/{}'.format(
            self.DB_TYPE, self.DB_USER, self.DB_PASS, self.DB_HOST, self.DB_PORT, self.DB_NAME)
