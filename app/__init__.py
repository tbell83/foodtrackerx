from os import getenv

from flask import Flask
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from app.config import Config

CONFIG = Config()
CONFIG.load(json_settings=getenv("CONFIG"))

APP = Flask(__name__)
DB = SQLAlchemy(APP)
MIGRATE = Migrate(APP, DB)
LOGIN = LoginManager(APP)
LOGIN.login_view = "login"

from app import routes
