""" Dreamhost Passenger Wrapper """
from os import execl, getenv, getcwd
from sys import argv, executable, path
from json import load

with open(getenv("CONFIG")) as settings_file:
    CONFIG = load(settings_file)
    INTERPRETER = CONFIG["INTERPRETER"]
settings_file.close()

if executable != INTERPRETER:
    execl(INTERPRETER, INTERPRETER, *argv)
path.append(getcwd())

from app import APP

application = APP
